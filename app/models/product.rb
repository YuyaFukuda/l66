class Product < ActiveRecord::Base
    has_one :cart_item
    validates :name,:price, presence: true
    validates :price, numericality: {only_integer: true, greater_than: 0}
end
